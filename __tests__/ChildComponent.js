import React from 'react';
import ChildComponent from '../src/ChildComponent';

const onSubmitSpy = jest.fn();
const onSubmit = onSubmitSpy;

const wrapper = shallow(<ChildComponent onSubmit={onSubmitSpy} />);
let container;
let containerButton;

describe('should have a <div> with properly className prop', () => {
  beforeEach(() => {
    container = wrapper.find('div');
    containerButton = container.find('button');
    // onSubmitSpy.mockClear();
  });

  it('should hav a <div>', () => {
    expect(container).toHaveLength(1);
  });

  it('should have a <div> with properly className prop', () => {
    expect(container.props().className).toEqual('container');
  });

  it('should have a <button>', () => {
    expect(containerButton).toHaveLength(1);
  });

  describe('<button> behaviour', () => {
    it('should call submit', () => {
      // expect(onSubmitSpy).not.toHaveBeenCalled();
      containerButton.simulate('click');
      expect(onSubmitSpy).toHaveBeenCalled();
      expect(onSubmitSpy).toHaveBeenCalledWith('I\'m your son');
    })
  })
})