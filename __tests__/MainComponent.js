import React from 'react';
import MainComponent from '../src/MainComponent';
import { JestEnvironment } from '@jest/environment';
import { isTSAnyKeyword, exportAllDeclaration } from '@babel/types';

jest.mock('../src/ChildComponent');

const wrapper = shallow(<MainComponent />);
let container
let containerProps
let childContainer;
let childContainerProps;

describe('MainComponent', () => {
  beforeEach(() => {
    container = wrapper.find('div');
    containerProps = container.props();
  });

  it('should have a <div>', () => {
    expect(container).toHaveLength(1);
  });

  it('should have a <div> with properly className prop', () => {
    expect(containerProps.className).toEqual('container')
  });

  describe('ChildComponent', () => {
    beforeEach(() => {
      childContainer = wrapper.find('ChildComponent');
      childContainerProps = childContainer.props();
    });

    it('should have a <ChildContainer>', () => {
      expect(childContainer).toHaveLength(1);
    });

    it('should have a label as props', () => {
      expect(childContainerProps.label).toEqual('I\'m your father');
    });

    it('should have onSubmit as prop', () => {
      expect(typeof childContainerProps.onSubmit).toBe('function')
    });
  })
})