import React from 'react';
import MyFirstComponent from '../src/MyFirstComponent';
import { isTSAnyKeyword, exportAllDeclaration } from '@babel/types';

const wrapper = shallow(<MyFirstComponent />)
let container;
let containerProps

describe('MyFirstComponent - No props', () => {
  it('should have state set properly', () => {
    expect(wrapper.state().buttonEnable).toEqual(true);
  });

  describe('render <div>', () => {
    beforeEach(() => {
      container = wrapper.find('div');
      containerProps = container.props();
    });

    it('should have a <div>', () => {
      expect(container).toHaveLength(1);
    });

    it('should have a <div> with property className prop', () => {
      expect(containerProps.className).toEqual('container');
    });
  });

  describe('render <h2>', () => {
    beforeEach(() => {
      container = wrapper.find('h2');
    });

    it('should have a <h2> with the property text', () => {
      expect(container.text()).toEqual('Hello World')
    });

    it('should have a <h2>', () => {
      expect(container).toHaveLength(1);
    });
  });

  describe('render <button>', () => {
    beforeEach(() => {
      container = wrapper.find('button');
      containerProps = container.props();
    });

    it('should have a <button>', () => {
      expect(container).toHaveLength(1);
    });

    it('should have a <button> with property text', () => {
      expect(container.text()).toEqual('click me');
    });

    it('should have <button> without disabled props', () => {
      expect(containerProps.disaabled).toEqual(undefined)
    });

    it('should change the state to false onClick <button>', () => {
      container.simulate('click');
      expect(wrapper.state().buttonEnable).toEqual(false);
    });
  });

  describe('MyFirstComponent - props', () => {
    const wrapper = shallow(<MyFirstComponent title='Bye Bye' />);
    it('should have a <h2> with the property text', () => {
      container = wrapper.find('h2');
      expect(container.text()).toEqual('Bye Bye');
    });
  });
});
